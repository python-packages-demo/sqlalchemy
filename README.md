# [SQLAlchemy](https://pypi.org/project/SQLAlchemy/)

SQL Toolkit and Object Relational Mapper https://sqlalchemy.org/

![PyPI - Downloads](https://img.shields.io/pypi/dm/sqlalchemy)
![sqlalchemy pepy.tech Downloads](https://static.pepy.tech/badge/sqlalchemy/month)

* [*An Introduction to SQLAlchemy in Python*](https://towardsdatascience.com/sql-and-etl-an-introduction-to-sqlalchemy-in-python-fc66e8be1cd4)